package com.inditex.pvp.application.mapper;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.inditex.pvp.FakeData;
import com.inditex.pvp.application.adapter.inbound.web.response.PriceResponse;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.PriceEntity;
import com.inditex.pvp.domain.Brand;
import com.inditex.pvp.domain.Currency;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.Product;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class PriceMapperTest {

    @Spy
    private PriceMapper mapper = Mappers.getMapper(PriceMapper.class);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should map Price to PriceResponse")
    void shouldMapPriceToPriceResponse() {
        final Price price = FakeData.buildFakePrice(1L, 1, BigDecimal.TEN,
                LocalDateTime.now().minusHours(24), LocalDateTime.now());
        final PriceResponse resp = mapper.mapModelToResponse(price);
        Assertions.assertNotNull(resp);
        Assertions.assertEquals(price.id(), resp.getId());
        Assertions.assertEquals(price.value(), resp.getValue());
        Assertions.assertEquals(price.startDateTime(), resp.getStartDateTime());
        Assertions.assertEquals(price.endDateTime(), resp.getEndDateTime());
        Assertions.assertEquals(price.currency().code(), resp.getCurrency());
        Assertions.assertEquals(price.brand().id(), resp.getBrandId());
        Assertions.assertEquals(price.product().id(), resp.getProductId());
    }

    @Test
    @DisplayName("Should not map Price to PriceResponse where source is null")
    void shouldNotMapPriceToPriceResponse_whenSourceIsNull() {
        Assertions.assertNull(mapper.mapModelToResponse(null));
        Assertions.assertNull(mapper.mapCurrencyModelToCurrencyCode(null));
        Assertions.assertNull(mapper.mapBrandModelToBrandId(null));
        Assertions.assertNull(mapper.mapProductModelToProductId(null));

    }

    @Test
    @DisplayName("Should map Price to PriceResponse when internal models are null")
    void shouldMapPriceToPriceResponse_whenInternalModelsAreNull() {
        final Price price = new Price(1L, LocalDateTime.now().minusHours(24),
                LocalDateTime.now(), 1, BigDecimal.TEN);
        final PriceResponse resp = mapper.mapModelToResponse(price);
        Assertions.assertNotNull(resp);
        Assertions.assertEquals(price.id(), resp.getId());
        Assertions.assertEquals(price.value(), resp.getValue());
        Assertions.assertEquals(price.startDateTime(), resp.getStartDateTime());
        Assertions.assertEquals(price.endDateTime(), resp.getEndDateTime());
        Assertions.assertNull(resp.getCurrency());
        Assertions.assertNull(resp.getBrandId());
        Assertions.assertNull(resp.getProductId());
    }

    @Test
    @DisplayName("Should map Price to PriceResponse when values are null")
    void shouldMapPriceToPriceResponse_whenValuesAreNull() {
        final Price price = new Price(new Brand(), new Product(), new Currency());
        final PriceResponse resp = mapper.mapModelToResponse(price);
        Assertions.assertNotNull(resp);
        Assertions.assertNull(resp.getId());
        Assertions.assertNull(resp.getValue());
        Assertions.assertNull(resp.getStartDateTime());
        Assertions.assertNull(resp.getEndDateTime());
        Assertions.assertNull(resp.getCurrency());
        Assertions.assertNull(resp.getBrandId());
        Assertions.assertNull(resp.getProductId());
    }

    @Test
    @DisplayName("Should map PriceEntity to Price")
    public void shouldMapPriceEntityToPrice() {
        final PriceEntity entity = FakeData.buildFakePriceEntity(1L, 1, BigDecimal.TEN,
                LocalDateTime.now().minusHours(24), LocalDateTime.now());
        final Price price = mapper.mapEntityToModel(entity);
        Assertions.assertNotNull(price);
        Assertions.assertEquals(entity.getId(), price.id());
        Assertions.assertEquals(entity.getValue(), price.value());
        Assertions.assertEquals(entity.getPriority(), price.priority());
        Assertions.assertEquals(entity.getStartDateTime(), price.startDateTime());
        Assertions.assertEquals(entity.getEndDateTime(), price.endDateTime());
        Assertions.assertEquals(entity.getCurrency().getId(), price.currency().id());
        Assertions.assertEquals(entity.getCurrency().getSymbol(), price.currency().symbol());
        Assertions.assertEquals(entity.getCurrency().getCode(), price.currency().code());
        Assertions.assertEquals(entity.getCurrency().getName(), price.currency().name());
        Assertions.assertEquals(entity.getBrand().getId(), price.brand().id());
        Assertions.assertEquals(entity.getBrand().getName(), price.brand().name());
        Assertions.assertEquals(entity.getProduct().getId(), price.product().id());
        Assertions.assertEquals(entity.getProduct().getName(), price.product().name());
    }

    @Test
    @DisplayName("Should not map PriceEntity to Price when source is null")
    public void shouldNotMapPriceEntityToPrice_whenSourceIsNull() {
        Assertions.assertNull(mapper.mapEntityToModel((PriceEntity) null));
    }

    @Test
    @DisplayName("Should map PriceEntity to Price when internal entities are null")
    public void shouldMapPriceEntityToPrice_whenInternalEntitiesAreNull() {
        final PriceEntity entity = FakeData.buildFakePriceEntity(1L, 1, BigDecimal.TEN,
                LocalDateTime.now().minusHours(24), LocalDateTime.now());
        entity.setCurrency(null);
        entity.setProduct(null);
        entity.setBrand(null);
        final Price price = mapper.mapEntityToModel(entity);
        Assertions.assertNotNull(price);
        Assertions.assertEquals(entity.getId(), price.id());
        Assertions.assertEquals(entity.getValue(), price.value());
        Assertions.assertEquals(entity.getPriority(), price.priority());
        Assertions.assertEquals(entity.getStartDateTime(), price.startDateTime());
        Assertions.assertEquals(entity.getEndDateTime(), price.endDateTime());
        Assertions.assertNull(price.currency());
        Assertions.assertNull(price.brand());
        Assertions.assertNull(price.product());;
    }

    @Test
    @DisplayName("Should map list of PriceEntity to list of Price")
    public void shouldMapListOfPriceEntityToListOfPrice() {
        final List<PriceEntity> entities = Lists.newArrayList(
                FakeData.buildFakePriceEntity(1L, 1, BigDecimal.TEN,
                        LocalDateTime.now().minusHours(24), LocalDateTime.now()),
                FakeData.buildFakePriceEntity(2L, 5, BigDecimal.ONE,
                        LocalDateTime.now().minusYears(1), LocalDateTime.now().minusDays(30)));
        final List<Price> prices = mapper.mapEntityToModel(entities);
        Assertions.assertEquals(entities.size(), prices.size());
        prices.forEach(price -> {
            Optional<PriceEntity> entityOpt = entities.stream().filter(el -> el.getId().equals(price.id())).findFirst();
            Assertions.assertTrue(entityOpt.isPresent());
            final PriceEntity entity = entityOpt.get();
            Assertions.assertEquals(entity.getId(), price.id());
            Assertions.assertEquals(entity.getValue(), price.value());
            Assertions.assertEquals(entity.getPriority(), price.priority());
            Assertions.assertEquals(entity.getStartDateTime(), price.startDateTime());
            Assertions.assertEquals(entity.getEndDateTime(), price.endDateTime());
            Assertions.assertEquals(entity.getCurrency().getId(), price.currency().id());
            Assertions.assertEquals(entity.getCurrency().getSymbol(), price.currency().symbol());
            Assertions.assertEquals(entity.getCurrency().getCode(), price.currency().code());
            Assertions.assertEquals(entity.getCurrency().getName(), price.currency().name());
            Assertions.assertEquals(entity.getBrand().getId(), price.brand().id());
            Assertions.assertEquals(entity.getBrand().getName(), price.brand().name());
            Assertions.assertEquals(entity.getProduct().getId(), price.product().id());
            Assertions.assertEquals(entity.getProduct().getName(), price.product().name());
        });
    }

    @Test
    @DisplayName("Should not map list of PriceEntity to list of Price when source is null")
    public void shouldNotMapListOfPriceEntityToListOfPrice_whenSourceIsNull() {
        Assertions.assertNull(mapper.mapEntityToModel((List) null));
    }

}
