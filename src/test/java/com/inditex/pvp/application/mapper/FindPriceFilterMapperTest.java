package com.inditex.pvp.application.mapper;

import java.time.LocalDateTime;

import com.inditex.pvp.FakeData;
import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class FindPriceFilterMapperTest {

    private static final LocalDateTime FAKE_APPLY_DATETIME =
            LocalDateTime.of(2020,6,14,10,0,0);

    @Spy
    private FindPriceFilterMapper mapper = Mappers.getMapper(FindPriceFilterMapper.class);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should map FindPriceFilterRequest to FindPriceFilterDTO")
    void shouldMapFindPriceFilterRequestToFindPriceFilterDTO() {
        final FindPriceFilterDTO dto = mapper.mapRequestToDto(FakeData.FAKE_BRAND_ID,
                FakeData.FAKE_PRODUCT_ID, FAKE_APPLY_DATETIME);
        Assertions.assertNotNull(dto);
        Assertions.assertEquals(FakeData.FAKE_BRAND_ID, dto.getBrandId());
        Assertions.assertEquals(FakeData.FAKE_PRODUCT_ID, dto.getProductId());
        Assertions.assertEquals(FAKE_APPLY_DATETIME, dto.getApplyDateTime());
    }

    @Test
    @DisplayName("Should not map FindPriceFilterRequest to FindPriceFilterDTO when source is null")
    void shouldNotMapFindPriceFilterRequestToFindPriceFilterDTO_whenSourceIsNull() {
        Assertions.assertNull(mapper.mapRequestToDto(null, null, null));
        Assertions.assertNotNull(mapper.mapRequestToDto(FakeData.FAKE_BRAND_ID, null, FAKE_APPLY_DATETIME));
        Assertions.assertNotNull(mapper.mapRequestToDto(null, FakeData.FAKE_PRODUCT_ID, null));
        Assertions.assertNotNull(mapper.mapRequestToDto(null, FakeData.FAKE_PRODUCT_ID, FAKE_APPLY_DATETIME));
        Assertions.assertNotNull(mapper.mapRequestToDto(null, null, FAKE_APPLY_DATETIME));
        Assertions.assertNotNull(mapper.mapRequestToDto(FakeData.FAKE_BRAND_ID, null, FAKE_APPLY_DATETIME));
    }

}
