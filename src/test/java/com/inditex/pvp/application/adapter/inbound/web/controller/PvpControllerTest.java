package com.inditex.pvp.application.adapter.inbound.web.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inditex.pvp.FakeData;
import com.inditex.pvp.application.adapter.inbound.web.PvpController;
import com.inditex.pvp.application.adapter.inbound.web.response.PriceResponse;
import com.inditex.pvp.application.mapper.FindPriceFilterMapper;
import com.inditex.pvp.application.mapper.PriceMapper;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.service.PriceService;
import com.inditex.pvp.infrasctruture.exception.ServiceException;
import com.inditex.pvp.infrasctruture.utils.ResourceBundle;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebMvcTest(controllers = PvpController.class)
@ExtendWith(SpringExtension.class)
public class PvpControllerTest {

    private static final String FAKE_APPLY_DATETIME = "2020-06-14T10:00:00";

    private final WebApplicationContext webApplicationContext;

    MockMvc mockMvc;

    @MockBean
    private PriceService service;

    @MockBean
    private PriceMapper priceMapper;

    @MockBean
    private FindPriceFilterMapper findPriceFilterMapper;

    @MockBean
    private ResourceBundle resourceBundle;

    public PvpControllerTest(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @BeforeEach
    void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    @DisplayName("Should find price - Success")
    void shouldFindPrice_success() throws Exception  {
        final Price price = FakeData.buildFakePrice(2L, 5, BigDecimal.ONE,
                LocalDateTime.now().minusDays(30), LocalDateTime.now());
        final PriceResponse priceResponse = PriceResponse.builder().id(price.id()).productId(price.product().id())
                .currency(price.currency().code()).brandId(price.brand().id()).value(price.value())
                .endDateTime(price.endDateTime()).startDateTime(price.startDateTime()).build();

        when(findPriceFilterMapper.mapRequestToDto(anyLong(), anyLong(), any())).thenReturn(FakeData.buildFakePriceFilterDto());
        when(service.findPrice(Mockito.any(FindPriceFilterDTO.class))).thenReturn(price);
        when(priceMapper.mapModelToResponse(Mockito.any(Price.class))).thenReturn(priceResponse);

        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", String.valueOf(FakeData.FAKE_BRAND_ID))
                .queryParam("product_id", String.valueOf(FakeData.FAKE_PRODUCT_ID))
                .queryParam("apply_datetime", FAKE_APPLY_DATETIME));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(priceResponse.getId().intValue())));
    }

    @Test
    @DisplayName("Should not find price - no records found - Fail")
    void shouldNotFindPrice_noRecordsFound_fail() throws Exception  {
        when(findPriceFilterMapper.mapRequestToDto(anyLong(), anyLong(), any())).thenReturn(FakeData.buildFakePriceFilterDto());
        when(service.findPrice(Mockito.any(FindPriceFilterDTO.class))).thenThrow(new ServiceException("No records found", HttpStatus.NOT_FOUND));
        when(priceMapper.mapModelToResponse(Mockito.any(Price.class))).thenReturn(null);
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", String.valueOf(FakeData.FAKE_BRAND_ID))
                .queryParam("product_id", String.valueOf(FakeData.FAKE_PRODUCT_ID))
                .queryParam("apply_datetime", FAKE_APPLY_DATETIME));
        response.andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    @DisplayName("Should throw unexpected error - Fail")
    void shouldThrowUnexpectedError_fail() throws Exception  {
        final Price price = FakeData.buildFakePrice(2L, 5, BigDecimal.ONE,
                LocalDateTime.now().minusDays(30), LocalDateTime.now());
        when(findPriceFilterMapper.mapRequestToDto(anyLong(), anyLong(), any())).thenReturn(FakeData.buildFakePriceFilterDto());
        when(service.findPrice(Mockito.any(FindPriceFilterDTO.class))).thenReturn(price);
        when(priceMapper.mapModelToResponse(Mockito.any(Price.class))).thenThrow(NullPointerException.class);
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", String.valueOf(FakeData.FAKE_BRAND_ID))
                .queryParam("product_id", String.valueOf(FakeData.FAKE_PRODUCT_ID))
                .queryParam("apply_datetime", FAKE_APPLY_DATETIME));
        response.andExpect(MockMvcResultMatchers.status().is5xxServerError());
    }

    @Test
    @DisplayName("Should cause bad request - Invalid Brand Id - Fail")
    void shouldCauseBadRequest_invalidBrandId_fail() throws Exception  {
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", "Invalid Id")
                .queryParam("product_id", String.valueOf(FakeData.FAKE_PRODUCT_ID))
                .queryParam("apply_datetime", FAKE_APPLY_DATETIME));
        response.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("Should cause bad request - Invalid Product Id - Fail")
    void shouldCauseBadRequest_invalidProductId_fail() throws Exception  {
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", String.valueOf(FakeData.FAKE_BRAND_ID))
                .queryParam("product_id", "Invalid Id")
                .queryParam("apply_datetime", FAKE_APPLY_DATETIME));
        response.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @DisplayName("Should cause bad request - Invalid Apply Date Time - Fail")
    void shouldCauseBadRequest_invalidApplyDateTime_fail() throws Exception  {
        ResultActions response = mockMvc.perform(MockMvcRequestBuilders.get("/pvp/price")
                .queryParam("brand_id", String.valueOf(FakeData.FAKE_BRAND_ID))
                .queryParam("product_id", String.valueOf(FakeData.FAKE_PRODUCT_ID))
                .queryParam("apply_datetime", "Invalid Date"));
        response.andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

}
