package com.inditex.pvp.application.adapter.outbound.persistence;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.inditex.pvp.FakeData;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.PriceEntity;
import com.inditex.pvp.application.adapter.outbound.persistence.repository.PriceRepository;
import com.inditex.pvp.application.mapper.PriceMapper;
import com.inditex.pvp.domain.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class PriceDAOAdapterTest {

    @Mock
    private PriceRepository repository;

    @Spy
    private PriceMapper mapper = Mappers.getMapper(PriceMapper.class);

    @InjectMocks
    private PriceDAOAdapter adapter;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should find prices by filter")
    public void shouldFindPricesByFilter() {
        final List<PriceEntity> entities = new ArrayList<>();
        entities.add(FakeData.buildFakePriceEntity(1L, 1, BigDecimal.TEN,
                LocalDateTime.now().minusDays(2), LocalDateTime.now()));
        entities.add(FakeData.buildFakePriceEntity(2L, 5, BigDecimal.ONE,
                LocalDateTime.now().minusDays(30), LocalDateTime.now()));

        when(repository.findPrices(anyLong(), anyLong(), any())).thenReturn(entities);

        final List<Price> actual = adapter.findPrices(FakeData.buildFakePriceFilterDto());

        Assertions.assertEquals(entities.size(), actual.size());
        actual.forEach(el1 -> {
            Optional<PriceEntity> entityOpt = entities.stream().filter(el2 ->
                    el2.getId().equals(el1.id())).findAny();
            Assertions.assertTrue(entityOpt.isPresent());
            PriceEntity entity = entityOpt.get();
            Assertions.assertEquals(entity.getValue(), el1.value());
            Assertions.assertEquals(entity.getPriority(), el1.priority());
            Assertions.assertEquals(entity.getEndDateTime(), el1.endDateTime());
            Assertions.assertEquals(entity.getStartDateTime(), el1.startDateTime());
            Assertions.assertEquals(entity.getBrand().getId(), el1.brand().id());
            Assertions.assertEquals(entity.getBrand().getName(), el1.brand().name());
            Assertions.assertEquals(entity.getCurrency().getCode(), el1.currency().code());
            Assertions.assertEquals(entity.getCurrency().getName(), el1.currency().name());
            Assertions.assertEquals(entity.getCurrency().getSymbol(), el1.currency().symbol());
            Assertions.assertEquals(entity.getProduct().getId(), el1.product().id());
            Assertions.assertEquals(entity.getProduct().getName(), el1.product().name());
        });
    }

}
