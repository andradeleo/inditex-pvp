package com.inditex.pvp.application.adapter.inbound.web.controller;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PvpControllerITCase {

    private static final String ENDPOINT_URL = "/pvp/price";
    private static final Long BRAND_ID = 1L;
    private static final Long PRODUCT_ID = 35455L;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void test_date_2020_06_14_at_time_10_00_00() throws Exception {
        doRestCallAndCheckResponseId("2020-06-14T10:00:00", 1L);
    }

    @Test
    public void test_date_2020_06_14_at_time_16_00_00() throws Exception {
        doRestCallAndCheckResponseId("2020-06-14T16:00:00", 2L);
    }

    @Test
    public void test_date_2020_06_14_at_time_21_00_00() throws Exception {
        doRestCallAndCheckResponseId("2020-06-14T21:00:00", 1L);
    }

    @Test
    public void test_date_2020_06_15_at_time_10_00_00() throws Exception {
        doRestCallAndCheckResponseId("2020-06-15T10:00:00", 3L);
    }

    @Test
    public void test_date_2020_06_16_at_time_21_00_00() throws Exception {
        doRestCallAndCheckResponseId("2020-06-16T21:00:00", 4L);
    }

    private void doRestCallAndCheckResponseId(final String applyDateTime, final Long expectedResponseId) throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(ENDPOINT_URL)
                        .queryParam("brand_id", String.valueOf(BRAND_ID))
                        .queryParam("product_id", String.valueOf(PRODUCT_ID))
                        .queryParam("apply_datetime", applyDateTime)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("\"id\":" + expectedResponseId)));
    }

}
