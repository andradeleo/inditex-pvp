package com.inditex.pvp.infrasctruture.utils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.MessageSource;

public class ResourceBundleTest {

    @Mock
    private MessageSource messageSource;

    @InjectMocks
    private ResourceBundle resourceBundle;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should get simple message")
    void shouldGetSimpleMessage() {
        final String expected = "No records found";
        when(messageSource.getMessage(anyString(), any(), any())).thenReturn(expected);
        final String actual = resourceBundle.getMessage("no.records.found");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Should get message with params")
    void shouldGetMessageWithParams() {
        final String expected = "No records found - param1";
        when(messageSource.getMessage(anyString(), any(), any())).thenReturn(expected);
        final String actual = resourceBundle.getMessage("no.records.found", "param1");
        Assertions.assertEquals(expected, actual);
    }

}
