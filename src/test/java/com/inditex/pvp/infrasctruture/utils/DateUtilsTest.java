package com.inditex.pvp.infrasctruture.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class DateUtilsTest {

    @Spy
    private DateUtils dateUtils;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should parse string to local date time")
    void shouldParseStringToLocalDateTime() {
        final String dateStr = "2020-06-14T10:00:00";
        final LocalDateTime actual = DateUtils.toLocalDatetime(dateStr);
        Assertions.assertEquals(LocalDateTime.of(2020, 6, 14, 10, 0, 0), actual);
    }

    @Test
    @DisplayName("Should not parse string to local date time - null string")
    void shouldNotParseStringToLocalDateTime_nullString() {
        Assertions.assertNull(DateUtils.toLocalDatetime(null));
    }

    @Test
    @DisplayName("Should not parse string to local date time - invalid string - fail")
    void shouldNotParseStringToLocalDateTime_invalidString_fail() {
        final String dateStr = "ABC";
        Assertions.assertThrows(DateTimeParseException.class, () -> DateUtils.toLocalDatetime(dateStr));
    }

}
