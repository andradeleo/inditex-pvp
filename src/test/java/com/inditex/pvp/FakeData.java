package com.inditex.pvp;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.inditex.pvp.application.adapter.outbound.persistence.entity.BrandEntity;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.CurrencyEntity;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.PriceEntity;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.ProductEntity;
import com.inditex.pvp.domain.Brand;
import com.inditex.pvp.domain.Currency;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.Product;
import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.enums.CurrencyEnum;

public class FakeData {

    public static final Long FAKE_PRODUCT_ID = 1L;

    public static final Long FAKE_BRAND_ID = 2L;

    public static FindPriceFilterDTO buildFakePriceFilterDto() {
        final FindPriceFilterDTO filter = new FindPriceFilterDTO();
        filter.setProductId(1L);
        filter.setBrandId(1L);
        filter.setApplyDateTime(LocalDateTime.now());
        return filter;
    }

    public static PriceEntity buildFakePriceEntity(final Long id, final Integer priority, final BigDecimal value,
                                                   final LocalDateTime startDateTime, final LocalDateTime endDateTime) {
        PriceEntity price = new PriceEntity();
        price.setId(id);
        price.setPriority(priority);
        price.setStartDateTime(startDateTime);
        price.setEndDateTime(endDateTime);
        price.setValue(value);
        BrandEntity brand = new BrandEntity();
        brand.setId(FAKE_BRAND_ID);
        brand.setName("Fake Brand");
        price.setBrand(brand);
        ProductEntity product = new ProductEntity();
        product.setId(FAKE_PRODUCT_ID);
        product.setName("Fake Product");
        price.setProduct(product);
        CurrencyEntity currency = new CurrencyEntity();
        currency.setId(1L);
        currency.setName("US Dollar");
        currency.setCode(CurrencyEnum.USD);
        currency.setSymbol("$");
        price.setCurrency(currency);
        return price;
    }

    public static Price buildFakePrice(final Long id, final Integer priority, final BigDecimal value,
                                      final LocalDateTime startDateTime, final LocalDateTime endDateTime) {
        return new Price(id, startDateTime, endDateTime, priority, value,
                new Brand(FAKE_BRAND_ID, "Fake Brand"), new Product(FAKE_PRODUCT_ID, "Fake Product"),
                new Currency(1L, "US Dollar", CurrencyEnum.USD, "$"));
    }

}
