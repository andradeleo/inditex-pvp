package com.inditex.pvp.domain.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.inditex.pvp.FakeData;
import com.inditex.pvp.application.adapter.outbound.persistence.PriceDAOAdapter;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.infrasctruture.exception.ServiceException;
import com.inditex.pvp.infrasctruture.utils.ResourceBundle;
import org.assertj.core.util.Lists;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class PriceServiceTest {

    @Mock
    private PriceDAOAdapter dao;

    @Mock
    private ResourceBundle resourceBundle;

    @InjectMocks
    private PriceService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    @DisplayName("Should find price by filter")
    void shouldFindPriceByFilter() throws ServiceException {
        final Price expectedPrice = FakeData.buildFakePrice(2L, 5, BigDecimal.ONE,
                LocalDateTime.now().minusDays(30), LocalDateTime.now());
        final List<Price> entities = Lists.newArrayList(
                FakeData.buildFakePrice(1L, 1, BigDecimal.TEN,
                        LocalDateTime.now().minusDays(2), LocalDateTime.now()),
                expectedPrice);

        when(dao.findPrices(any())).thenReturn(entities);
        when(resourceBundle.getMessage(anyString())).thenReturn("No records found");

        final Price price = service.findPrice(FakeData.buildFakePriceFilterDto());

        Assertions.assertEquals(expectedPrice.id(), price.id());
        Assertions.assertEquals(expectedPrice.priority(), price.priority());
        Assertions.assertEquals(expectedPrice.value(), price.value());
        Assertions.assertEquals(expectedPrice.startDateTime(), price.startDateTime());
        Assertions.assertEquals(expectedPrice.endDateTime(), price.endDateTime());
        Assertions.assertEquals(expectedPrice.brand().id(), price.brand().id());
        Assertions.assertEquals(expectedPrice.brand().name(), price.brand().name());
        Assertions.assertEquals(expectedPrice.product().id(), price.product().id());
        Assertions.assertEquals(expectedPrice.product().name(), price.product().name());
        Assertions.assertEquals(expectedPrice.currency().id(), price.currency().id());
        Assertions.assertEquals(expectedPrice.currency().code(), price.currency().code());
        Assertions.assertEquals(expectedPrice.currency().name(), price.currency().name());
        Assertions.assertEquals(expectedPrice.currency().symbol(), price.currency().symbol());
    }

    @Test
    @DisplayName("Should not find price by filter - no records found")
    void shouldNotFindPriceByFilter_noRecordsFound() {
        when(dao.findPrices(any())).thenReturn(Lists.newArrayList());
        Assert.assertThrows(ServiceException.class, () ->
                service.findPrice(FakeData.buildFakePriceFilterDto()));
    }

    @Test
    @DisplayName("Should not find price by filter - unexpected error")
    void shouldNotFindPriceByFilter_unexpectedError() {
        when(dao.findPrices(any())).thenReturn(null);
        Assert.assertThrows(ServiceException.class, () ->
                service.findPrice(FakeData.buildFakePriceFilterDto()));
    }

}
