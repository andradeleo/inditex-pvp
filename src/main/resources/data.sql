-- Load Currencies
insert into TB_CURRENCY (ID, NAME, CODE, SYMBOL, ENABLED, CREATION_DATE) values (1, 'Euro', 'EUR', '€', true, now());
insert into TB_CURRENCY (ID, NAME, CODE, SYMBOL, ENABLED, CREATION_DATE) values (2, 'Dollar', 'USD', '$', true, now());
insert into TB_CURRENCY (ID, NAME, CODE, SYMBOL, ENABLED, CREATION_DATE) values (3, 'Real', 'BRL', 'R$', true, now());

-- Load Brands
insert into TB_BRAND (ID, NAME, ENABLED, CREATION_DATE) values (1, 'Zara', true, now());

-- Load Products
insert into TB_PRODUCT (ID, NAME, ENABLED, CREATION_DATE) values (35455, 'Suit Wool Slim Style', true, now());

-- Load Prices
insert into TB_PRICE (ID, START_DATE, END_DATE, PRIORITY, "VALUE", BRAND_ID, PRODUCT_ID, CURRENCY_ID, ENABLED, CREATION_DATE) values (1, '2020-06-14 00:00:00', '2020-12-31 23:59:59', 0, 35.50, 1, 35455, 1, true, now());
insert into TB_PRICE (ID, START_DATE, END_DATE, PRIORITY, "VALUE", BRAND_ID, PRODUCT_ID, CURRENCY_ID, ENABLED, CREATION_DATE) values (2, '2020-06-14 15:00:00', '2020-06-14 18:30:00', 1, 25.45, 1, 35455, 1, true, now());
insert into TB_PRICE (ID, START_DATE, END_DATE, PRIORITY, "VALUE", BRAND_ID, PRODUCT_ID, CURRENCY_ID, ENABLED, CREATION_DATE) values (3, '2020-06-15 00:00:00', '2020-06-15 11:00:00', 1, 30.50, 1, 35455, 1, true, now());
insert into TB_PRICE (ID, START_DATE, END_DATE, PRIORITY, "VALUE", BRAND_ID, PRODUCT_ID, CURRENCY_ID, ENABLED, CREATION_DATE) values (4, '2020-06-15 16:00:00', '2020-12-31 23:59:59', 1, 38.95, 1, 35455, 1, true, now());

