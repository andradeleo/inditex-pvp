package com.inditex.pvp.application.adapter.outbound.persistence.repository;

import com.inditex.pvp.application.adapter.outbound.persistence.entity.PriceEntity;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

    @Query("select e from PriceEntity e where e.enabled = true " +
            " and e.product.id = :productId and e.brand.id = :brandId " +
            " and :applyDateTime >= e.startDateTime and :applyDateTime <= e.endDateTime order by e.id")
    List<PriceEntity> findPrices(@Param("brandId") Long brandId,
                                 @Param("productId") Long productId,
                                 @Param("applyDateTime") LocalDateTime applyDateTime) throws DataAccessException;
}
