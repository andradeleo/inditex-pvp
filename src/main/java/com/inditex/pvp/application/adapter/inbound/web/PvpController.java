package com.inditex.pvp.application.adapter.inbound.web;

import java.time.LocalDateTime;

import com.inditex.pvp.application.adapter.inbound.web.response.PriceResponse;
import com.inditex.pvp.application.mapper.FindPriceFilterMapper;
import com.inditex.pvp.application.mapper.PriceMapper;
import com.inditex.pvp.domain.port.service.PriceServicePort;
import com.inditex.pvp.infrasctruture.exception.ResourceException;
import com.inditex.pvp.infrasctruture.exception.ServiceException;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("pvp")
@AllArgsConstructor
public class PvpController {

    private final PriceServicePort priceService;

    private final PriceMapper priceMapper;

    private final FindPriceFilterMapper findPriceFilterMapper;

    @GetMapping("/price")
    public ResponseEntity<PriceResponse> findPrice(
            @ApiParam(name =  "brand_id", type = "Long", value = "Numeric brand identifier", example = "1", required = true)
            @RequestParam("brand_id") Long brandId,
            @ApiParam(name =  "product_id", type = "Long", value = "Numeric product identifier", example = "35455", required = true)
            @RequestParam("product_id") Long productId,
            @ApiParam(name =  "apply_datetime", type = "LocalDateTime", value = "DateTime reference for the price", example = "2020-06-14T16:00:00", required = true)
            @RequestParam("apply_datetime")
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime applyDateTime) throws ResourceException {
        try {
            return ResponseEntity.ok(priceMapper.mapModelToResponse(
                    priceService.findPrice(findPriceFilterMapper.mapRequestToDto(brandId, productId, applyDateTime))));
        } catch (ServiceException e) {
            log.error(e.getMessage(), e);
            throw new ResourceException(e.getMessage(), e.getHttpStatus());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ResourceException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
