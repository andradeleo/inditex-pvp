package com.inditex.pvp.application.adapter.outbound.persistence.entity;

import java.io.Serial;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TB_BRAND")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BrandEntity extends AbstractEntity<Long> {

    @Serial
    private static final long serialVersionUID = 1L;

    @Column(name = "NAME", length = 80, nullable = false)
    private String name;

}
