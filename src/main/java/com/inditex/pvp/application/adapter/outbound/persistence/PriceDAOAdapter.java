package com.inditex.pvp.application.adapter.outbound.persistence;

import java.util.List;

import com.inditex.pvp.application.adapter.outbound.persistence.repository.PriceRepository;
import com.inditex.pvp.application.mapper.PriceMapper;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.port.dao.PriceDAOPort;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
@AllArgsConstructor
public class PriceDAOAdapter implements PriceDAOPort {

    private final PriceRepository priceRepository;

    private final PriceMapper priceMapper;

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<Price> findPrices(FindPriceFilterDTO filter) {
        return priceMapper.mapEntityToModel(
                priceRepository.findPrices(filter.getBrandId(), filter.getProductId(),
                        filter.getApplyDateTime()));
    }
}
