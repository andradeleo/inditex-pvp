package com.inditex.pvp.application.adapter.outbound.persistence.entity;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class AbstractEntity<T> implements Serializable  {

    @Serial
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    @EqualsAndHashCode.Include
    protected T id;

    @Column(name = "ENABLED", length = 1, nullable = false)
    protected Boolean enabled = Boolean.TRUE;

    @Column(name = "CREATION_DATE", nullable = false)
    protected LocalDateTime creationDate;

    @Column(name = "LAST_UPD_DATE")
    protected LocalDateTime lastUpdDate;

    @PrePersist
    public void prePersist() {
        enabled = Boolean.TRUE;
        creationDate = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
    }

    @PreUpdate
    public void preUpdate() {
        lastUpdDate = ZonedDateTime.now(ZoneOffset.UTC).toLocalDateTime();
    }

}
