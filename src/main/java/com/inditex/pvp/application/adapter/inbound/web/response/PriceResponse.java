package com.inditex.pvp.application.adapter.inbound.web.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.inditex.pvp.domain.enums.CurrencyEnum;
import com.inditex.pvp.application.utils.CustomLocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Builder
@ApiModel(description = "Class representing a price response in the application.")
public class PriceResponse {

    @ApiModelProperty(notes = "Record identifier.")
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(notes = "Numeric product identifier.")
    @JsonProperty("product_id")
    private Long productId;

    @ApiModelProperty(notes = "Numeric brand identifier.")
    @JsonProperty("brand_id")
    private Long brandId;

    @ApiModelProperty(notes = "Beginning of the price term.", example = "2020-06-14T16:00:00")
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonProperty("start_datetime")
    private LocalDateTime startDateTime;

    @ApiModelProperty(notes = "Ending of the price term.", example = "2020-06-14T16:00:00")
    @JsonSerialize(using = CustomLocalDateTimeSerializer.class)
    @JsonProperty("end_datetime")
    private LocalDateTime endDateTime;

    @ApiModelProperty(notes = "Price value amount.")
    @JsonProperty("value")
    private BigDecimal value;

    @ApiModelProperty(notes = "Price value currency.")
    @JsonProperty("currency")
    private CurrencyEnum currency;

}