package com.inditex.pvp.application.adapter.outbound.persistence.entity;

import java.io.Serial;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TB_PRICE")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class PriceEntity extends AbstractEntity<Long> {

    @Serial
    private static final long serialVersionUID = 1L;

    @Column(name = "START_DATE", nullable = false)
    private LocalDateTime startDateTime;

    @Column(name = "END_DATE", nullable = false)
    private LocalDateTime endDateTime;

    @Column(name = "PRIORITY", nullable = false)
    private Integer priority;

    @Column(name = "VALUE", nullable = false)
    private BigDecimal value;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(
            name = "BRAND_ID",
            referencedColumnName = "ID",
            nullable = false,
            foreignKey = @ForeignKey(name="FK_PRIC_BRAN"))
    private BrandEntity brand;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(
            name = "PRODUCT_ID",
            referencedColumnName = "ID",
            nullable = false,
            foreignKey = @ForeignKey(name="FK_PRIC_PROD"))
    private ProductEntity product;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(
            name = "CURRENCY_ID",
            referencedColumnName = "ID",
            nullable = false,
            foreignKey = @ForeignKey(name="FK_PRIC_CURR"))
    private CurrencyEntity currency;

}
