package com.inditex.pvp.application.adapter.outbound.persistence.entity;

import java.io.Serial;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.inditex.pvp.domain.enums.CurrencyEnum;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "TB_CURRENCY")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class CurrencyEntity extends AbstractEntity<Long> {

    @Serial
    private static final long serialVersionUID = 1L;

    @Column(name = "NAME", length = 80, nullable = false)
    private String name;

    @Column(name = "CODE", length = 5, nullable = false)
    @Enumerated(EnumType.STRING)
    private CurrencyEnum code;

    @Column(name = "SYMBOL", length = 3, nullable = false)
    private String symbol;

}
