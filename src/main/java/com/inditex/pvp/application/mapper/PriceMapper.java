package com.inditex.pvp.application.mapper;

import java.util.List;

import com.inditex.pvp.application.adapter.inbound.web.response.PriceResponse;
import com.inditex.pvp.application.adapter.outbound.persistence.entity.PriceEntity;
import com.inditex.pvp.domain.Brand;
import com.inditex.pvp.domain.Currency;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.Product;
import com.inditex.pvp.domain.enums.CurrencyEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PriceMapper {

    @Mapping(target = "currency", expression = "java(mapCurrencyModelToCurrencyCode(source))")
    @Mapping(target = "brandId", expression = "java(mapBrandModelToBrandId(source))")
    @Mapping(target = "productId", expression = "java(mapProductModelToProductId(source))")
    PriceResponse mapModelToResponse(Price source);

    Price mapEntityToModel(PriceEntity source);

    List<Price> mapEntityToModel(List<PriceEntity> source);

    default CurrencyEnum mapCurrencyModelToCurrencyCode(Price price) {
        if (price == null)
            return null;
        if (price.currency() == null)
            return null;
        return price.currency().code();
    }

    default Long mapBrandModelToBrandId(Price price) {
        if (price == null)
            return null;
        if (price.brand() == null)
            return null;
        return price.brand().id();
    }

    default Long mapProductModelToProductId(Price price) {
        if (price == null)
            return null;
        if (price.product() == null)
            return null;
        return price.product().id();
    }

}
