package com.inditex.pvp.application.mapper;

import java.time.LocalDateTime;

import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.infrasctruture.utils.DateUtils;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, imports = DateUtils.class)
public interface FindPriceFilterMapper {

    FindPriceFilterDTO mapRequestToDto(Long brandId, Long productId, LocalDateTime applyDateTime);

}
