package com.inditex.pvp.domain.service;

import java.util.Comparator;

import com.inditex.pvp.domain.Price;
import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.port.dao.PriceDAOPort;
import com.inditex.pvp.domain.port.service.PriceServicePort;
import com.inditex.pvp.infrasctruture.exception.BusinessException;
import com.inditex.pvp.infrasctruture.exception.ServiceException;
import com.inditex.pvp.infrasctruture.utils.ResourceBundle;
import org.springframework.http.HttpStatus;

public class PriceService implements PriceServicePort {

    private final Integer HTTP_STATUS_404 = 404;

    private final PriceDAOPort priceDAO;

    private final ResourceBundle resourceBundle;

    public PriceService(PriceDAOPort priceDAO, ResourceBundle resourceBundle) {
        this.priceDAO = priceDAO;
        this.resourceBundle = resourceBundle;
    }

    @Override
    public Price findPrice(FindPriceFilterDTO req) throws ServiceException {
        try {
            return priceDAO.findPrices(req).stream().max(
                    Comparator.comparing(Price::priority)).orElseThrow(
                                    () -> new BusinessException(
                                            resourceBundle.getMessage("result.not.found"), HTTP_STATUS_404));
        } catch (BusinessException e) {
            throw new ServiceException(e.getMessage(), e.getHttpStatus());
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

}
