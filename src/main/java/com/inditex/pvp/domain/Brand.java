package com.inditex.pvp.domain;

public record Brand(Long id, String name) {

    public Brand() {
        this(null, null);
    }

}
