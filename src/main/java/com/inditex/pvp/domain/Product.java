package com.inditex.pvp.domain;

public record Product(Long id, String name) {

    public Product() {
        this(null, null);
    }

}
