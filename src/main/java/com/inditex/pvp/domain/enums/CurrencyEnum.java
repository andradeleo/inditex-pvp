package com.inditex.pvp.domain.enums;

public enum CurrencyEnum {
    EUR, USD, BRL
}
