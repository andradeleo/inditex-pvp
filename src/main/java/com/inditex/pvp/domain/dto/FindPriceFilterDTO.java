package com.inditex.pvp.domain.dto;

import java.time.LocalDateTime;

public class FindPriceFilterDTO {

    private Long brandId;

    private Long productId;

    private LocalDateTime applyDateTime;

    public FindPriceFilterDTO() {
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public LocalDateTime getApplyDateTime() {
        return applyDateTime;
    }

    public void setApplyDateTime(LocalDateTime applyDateTime) {
        this.applyDateTime = applyDateTime;
    }
}
