package com.inditex.pvp.domain.port.dao;

import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.Price;

import java.util.List;

public interface PriceDAOPort {

    List<Price> findPrices(FindPriceFilterDTO filter);

}
