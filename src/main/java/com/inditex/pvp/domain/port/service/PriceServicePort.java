package com.inditex.pvp.domain.port.service;

import com.inditex.pvp.domain.dto.FindPriceFilterDTO;
import com.inditex.pvp.domain.Price;
import com.inditex.pvp.infrasctruture.exception.ServiceException;

public interface PriceServicePort {

    Price findPrice(FindPriceFilterDTO req) throws ServiceException;

}
