package com.inditex.pvp.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Price(Long id, LocalDateTime startDateTime, LocalDateTime endDateTime, Integer priority,
                    BigDecimal value, Brand brand, Product product, Currency currency) {

    public Price(Long id, LocalDateTime startDateTime, LocalDateTime endDateTime, Integer priority, BigDecimal value) {
        this(id, startDateTime, endDateTime, priority, value, null, null, null);
    }

    public Price(Brand brand, Product product, Currency currency) {
        this(null, null, null, null, null, brand, product, currency);
    }
}
