package com.inditex.pvp.domain;

import com.inditex.pvp.domain.enums.CurrencyEnum;

public record Currency(Long id, String name, CurrencyEnum code, String symbol) {

    public Currency() {
        this(null, null, null, null);
    }

}
