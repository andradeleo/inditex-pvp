package com.inditex.pvp.infrasctruture.exception.handler;

import java.util.Objects;

import com.inditex.pvp.infrasctruture.exception.ResourceException;
import com.inditex.pvp.infrasctruture.utils.ResourceBundle;
import lombok.AllArgsConstructor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@AllArgsConstructor
public class ResourceExceptionHandler extends ResponseEntityExceptionHandler {

    private final ResourceBundle resourceBundle;

    @ExceptionHandler(ResourceException.class)
    public final ResponseEntity<Object> handleResourceException(ResourceException exception) {
        return buildResponse(exception);
    }

    private ResponseEntity<Object> buildResponse(ResourceException exception) {
        if (Objects.equals(exception.getHttpStatus(), HttpStatus.INTERNAL_SERVER_ERROR)) {
            return new ResponseEntity<>(
                    ResourceExceptionMessage.builder().errorMessage(resourceBundle.getMessage("unexpected.error"))
                            .statusCode(exception.getHttpStatus().value()).build(),
                    exception.getHttpStatus()
            );
        }
        return new ResponseEntity<>(
                ResourceExceptionMessage.builder().errorMessage(exception.getMessage())
                        .statusCode(exception.getHttpStatus().value()).build(),
                exception.getHttpStatus()
        );
    }

}