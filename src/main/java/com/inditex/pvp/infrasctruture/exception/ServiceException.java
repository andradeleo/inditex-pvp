package com.inditex.pvp.infrasctruture.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ServiceException extends Exception {

    private final HttpStatus httpStatus;

    public ServiceException(String msg, HttpStatus status) {
        super(msg);
        this.httpStatus = status;
    }

    public ServiceException(Exception e) {
        super(e);
        this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
