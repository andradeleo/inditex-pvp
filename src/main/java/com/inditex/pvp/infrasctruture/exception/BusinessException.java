package com.inditex.pvp.infrasctruture.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BusinessException extends Exception {

    private final HttpStatus httpStatus;

    public BusinessException(String msg, Integer statusCode) {
        super(msg);
        this.httpStatus = HttpStatus.valueOf(statusCode);
    }

}