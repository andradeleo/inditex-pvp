package com.inditex.pvp.infrasctruture.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ResourceException extends Exception {

    private final HttpStatus httpStatus;

    public ResourceException(String msg, HttpStatus status) {
        super(msg);
        this.httpStatus = status;
    }

}