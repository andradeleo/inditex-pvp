package com.inditex.pvp.infrasctruture.utils;

import java.util.Locale;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ResourceBundle {

    private final MessageSource messageSource;

    public String getMessage(String key) {
        return messageSource.getMessage(key, null, Locale.getDefault());
    }

    public String getMessage(String key, String... params) {
        return messageSource.getMessage(key, params, Locale.getDefault());
    }

}
