package com.inditex.pvp.infrasctruture.utils;

import java.time.LocalDateTime;

public class DateUtils {

    public static LocalDateTime toLocalDatetime(String value) {
        if (value == null)
            return null;
        return LocalDateTime.parse(value);
    }
}
