package com.inditex.pvp.infrasctruture.config;

import com.inditex.pvp.domain.port.dao.PriceDAOPort;
import com.inditex.pvp.domain.port.service.PriceServicePort;
import com.inditex.pvp.domain.service.PriceService;
import com.inditex.pvp.infrasctruture.utils.ResourceBundle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfig  {

    @Bean
    public PriceServicePort priceService(PriceDAOPort priceDAO, ResourceBundle resourceBundle) {
        return new PriceService(priceDAO, resourceBundle);
    }

}
