# PVP INDITEX - TECHNICAL TEST 

This is a spring-boot application developed to meet the technical test requirements for Inditex's hiring process.

## Getting started

To compile and test the code, you have to execute the command line as bellow:

```batch
mvn clean install
```

## Running Only Integration Tests (Not Mocked)

To run only the integration tests, please execute the following command line:

```batch
mvn -Dskip.ut=true verify
```

## Running Unit Tests and Integration Tests

To run both Unit Tests and Integration Tests, please execute the following command line:

```batch
mvn verify
```

## Running

To start the application execute the following command line:

```batch
mvn spring-boot:run
```

## Swagger API documentation

- To see the api documentation, please access the following url (after launching the application in your localhost):
```batch
http://localhost:8080/swagger-ui/
```